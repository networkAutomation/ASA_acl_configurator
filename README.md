#### DEPENDENCIES

`sudo pip3 install netmiko python-handler-socket pyyaml multiprocessing threading ipaddress getpass3`

#### Source/Destination IP, protocol and TCP/UDP prt YAML file example

```
- SNOW:
    change: CHG0103393

- ipSource:
    address: 10.42.9.62
    mask: 255.255.255.255

- ipSource:
    address: 10.40.13.59
    mask: 255.255.255.255

- ipDestination:
    address: 4.4.4.4
    mask: 255.255.255.255

- ipDestination:
    address: 8.8.8.0
    mask: 255.255.255.0

- protPortDestination:
    protocol: tcp
    ports:
      - 80
      - 443
      - 8080

- protPortDestination:
    protocol: udp
    ports:
      - 53
      - 514
```

### firewall list YAML file example

```
  8.8.0.0/16: ASAv1
  4.0.0.0/8: ASAv0
```

### HELP MENU

```
usage: aclConfigurator.py [-h] [--data-input DATA_INPUT]
                          [--firewall-list FIREWALL_LIST] [--check]
                          [--execute]

Cisco ASA ACL rules configurator - Version 2.2 f180510

optional arguments:
  -h, --help            show this help message and exit
  --data-input DATA_INPUT, -d DATA_INPUT
                        YAML file data with source, destination IP, ports and
                        protocol
  --firewall-list FIREWALL_LIST, -f FIREWALL_LIST
                        YAML file with firewalls list and corresping
                        connected/static subnets
  --check, -C           Generate the config only - don't write config on the
                        firewall
  --execute, -X         Generate the config and copy on the firewall
```

### USAGE

Make sure you have your `firewallList.yaml` in the same directory you run the script

`python3 aclConfigurator.py -d CHG0107388.yaml -f firewallList.yaml -X`

```
object-group network H-10.42.9.62-32
  network-object host 10.42.9.62
object-group network H-10.40.13.59-32
  network-object host 10.40.13.59
object-group network N-10.42.9.0-27
  network-object 10.42.9.0 255.255.255.224
object-group network N-10.40.13.0-26
  network-object 10.40.13.0 255.255.255.192
object-group network N-10.40.9.0-24
  network-object 10.40.9.0 255.255.255.0
object-group network H-4.4.4.4-32
  network-object host 4.4.4.4
object-group network SG-CHG0103393
  group-object H-10.42.9.62-32
  group-object H-10.40.13.59-32
  group-object N-10.42.9.0-27
  group-object N-10.40.13.0-26
  group-object N-10.40.9.0-24
object-group network DG-CHG0103393
  group-object H-4.4.4.4-32
access-list transit-in extended permit tcp object-group SG-CHG0103393 object-group DG-CHG0103393 eq 80 log
access-list transit-in extended permit tcp object-group SG-CHG0103393 object-group DG-CHG0103393 eq 443 log
access-list transit-in extended permit tcp object-group SG-CHG0103393 object-group DG-CHG0103393 eq 8080 log
access-list transit-in extended permit udp object-group SG-CHG0103393 object-group DG-CHG0103393 eq 53 log
access-list transit-in extended permit udp object-group SG-CHG0103393 object-group DG-CHG0103393 eq 514 log

Do you want proceed on ASAv0? (yes/no): yes
Username: olivierif
Password:
Enable password:

########## CONFIGURATION CHECK ##########

show running-config object-group id SG-CHG0103393
object-group network SG-CHG0103393
 group-object H-10.42.9.62-32
 group-object H-10.40.13.59-32
 group-object N-10.42.9.0-27
 group-object N-10.40.13.0-26
 group-object N-10.40.9.0-24

show running-config object-group id DG-CHG0103393
object-group network DG-CHG0103393
 group-object H-4.4.4.4-32

show running-config access-list | i CHG0103393
access-list transit-in extended permit tcp object-group SG-CHG0103393 object-group DG-CHG0103393 eq www log
access-list transit-in extended permit tcp object-group SG-CHG0103393 object-group DG-CHG0103393 eq https log
access-list transit-in extended permit tcp object-group SG-CHG0103393 object-group DG-CHG0103393 eq 8080 log
access-list transit-in extended permit udp object-group SG-CHG0103393 object-group DG-CHG0103393 eq domain log
access-list transit-in extended permit udp object-group SG-CHG0103393 object-group DG-CHG0103393 eq syslog log

########## CONFIGURATION CHECK ##########

[...]

```
