#!/usr/bin/env python3

import yaml, sys, ipaddress, socket, getpass, argparse
from netmiko import ConnectHandler
from netaddr import IPAddress

objGrSList = []
objGrDList = []
finalConf = []
dstAddress = []

def queryYesNo(question, default="yes"):
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: ".format(default))

    while True:
        print(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' ""(or 'y' or 'n').\n")

def main():
    # flags
    parser = argparse.ArgumentParser(description="Cisco ASA ACL rules configurator - Version 2.7 f180626")
    parser.add_argument('--data-input', '-d', dest='data_input', help="YAML file data with source, destination IP, ports and protocol",type= str)
    parser.add_argument('--firewall-list','-f', dest='firewall_list', help="YAML file with firewalls list and corresping connected/static subnets", type= str)
    parser.add_argument('--check', '-C', dest='check', action='store_true',help="Generate the config only - don't write config to the firewall")
    parser.add_argument('--execute', '-X', dest='execute', action='store_true', help="Generate the config and copy it to the firewall")

    args = parser.parse_args()
    inventory = args.data_input
    firewalls = args.firewall_list
    check = args.check
    execute = args.execute

    if not firewalls or not inventory:
        print ('Please indicate the yaml files with the list of firewalls and the data-input file. i.e. "python3 aclConfigurator.py -d CHG0107388.yaml -f firewallList.yaml -C"')
        sys.exit(1)

    inventory = yaml.load(open(inventory, 'rb'))
    firewalls = yaml.load(open(firewalls, 'rb'))

    if not check or execute:
        print ("Please indicate -C or -X")

    # Create destination object-group
    if check:
        for destinations in inventory[2]['Destinations']:
            if destinations['ipDestination']['mask'] == '255.255.255.255':
                for ip in destinations['ipDestination']['address']:
                    finalConf.append('object-group network H-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                    objGrDList.append('H-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                    finalConf.append('  network-object host {}'.format(ip))
                    dstAddress.append(ip)
            else:
                for ip in destinations['ipDestination']['address']:
                    finalConf.append('object-group network N-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                    objGrDList.append('N-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                    finalConf.append('  network-object {} {}'.format(ip, destinations['ipDestination']['mask']))
                    dstAddress.append(ip)

            # Create source object-group
            for sources in inventory[1]['Sources']:
                for ip in sources['ipSource']['address']:
                    if sources['ipSource']['mask'] == '255.255.255.255':
                        finalConf.append('object-group network H-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                        objGrSList.append('H-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                        finalConf.append('  network-object host {}'.format(ip))
                    else:
                        finalConf.append('object-group network N-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                        objGrSList.append('N-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                        finalConf.append('  network-object {} {}'.format(ip, sources['ipSource']['mask']))

            # Create destination object-group network
            finalConf.append('object-group network DG-{}'.format(inventory[0]['SNOW']['change']))
            DGLite = ('DG-{}'.format(inventory[0]['SNOW']['change']))
            for i in objGrDList:
                finalConf.append('  group-object {}'.format(i))
            objGrDList.clear()

            # Create source object-group network
            finalConf.append('object-group network SG-{}'.format(inventory[0]['SNOW']['change']))
            SGLite = ('SG-{}'.format(inventory[0]['SNOW']['change']))
            for i in objGrSList:
                finalConf.append('  group-object {}'.format(i))
            objGrSList.clear()

            # Create ACLs
            for key in inventory:
                if 'protPortDestination' in key:
                    if key['protPortDestination']['protocol'] == 'tcp':
                        for i in key['protPortDestination']['ports']:
                            finalConf.append('access-list transit-in extended permit tcp object-group {} object-group {} eq {} log'.format(SGLite,DGLite,i))
                    elif key['protPortDestination']['protocol'] == 'udp':
                        for j in key['protPortDestination']['ports']:
                            finalConf.append('access-list transit-in extended permit udp object-group {} object-group {} eq {} log'.format(SGLite,DGLite,j))

            # config check
        for config in finalConf:
            print(config)

    if execute:
        for destinations in inventory[2]['Destinations']:
            if destinations['ipDestination']['mask'] == '255.255.255.255':
                for ip in destinations['ipDestination']['address']:
                    finalConf.append('object-group network H-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                    objGrDList.append('H-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                    finalConf.append('  network-object host {}'.format(ip))
                    dstAddress.append(ip)
            else:
                for ip in destinations['ipDestination']['address']:
                    finalConf.append('object-group network N-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                    objGrDList.append('N-{}-{}'.format(ip,IPAddress(destinations['ipDestination']['mask']).netmask_bits()))
                    finalConf.append('  network-object {} {}'.format(ip, destinations['ipDestination']['mask']))
                    dstAddress.append(ip)

                # Create source object-group
            for sources in inventory[1]['Sources']:
                for ip in sources['ipSource']['address']:
                    if sources['ipSource']['mask'] == '255.255.255.255':
                        finalConf.append('object-group network H-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                        objGrSList.append('H-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                        finalConf.append('  network-object host {}'.format(ip))
                    else:
                        finalConf.append('object-group network N-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                        objGrSList.append('N-{}-{}'.format(ip,IPAddress(sources['ipSource']['mask']).netmask_bits()))
                        finalConf.append('  network-object {} {}'.format(ip, sources['ipSource']['mask']))

                # Create destination object-group network
            finalConf.append('object-group network DG-{}'.format(inventory[0]['SNOW']['change']))
            DGLite = ('DG-{}'.format(inventory[0]['SNOW']['change']))
            for i in objGrDList:
                finalConf.append('  group-object {}'.format(i))
            objGrDList.clear()

            # Create source object-group network
            finalConf.append('object-group network SG-{}'.format(inventory[0]['SNOW']['change']))
            SGLite = ('SG-{}'.format(inventory[0]['SNOW']['change']))
            for i in objGrSList:
                finalConf.append('  group-object {}'.format(i))
            objGrSList.clear()

            # Create ACLs
            for key in inventory:
                if 'protPortDestination' in key:
                    if key['protPortDestination']['protocol'] == 'tcp':
                        for i in key['protPortDestination']['ports']:
                            finalConf.append('access-list transit-in extended permit tcp object-group {} object-group {} eq {} log'.format(SGLite,DGLite,i))
                    elif key['protPortDestination']['protocol'] == 'udp':
                        for j in key['protPortDestination']['ports']:
                            finalConf.append('access-list transit-in extended permit udp object-group {} object-group {} eq {} log'.format(SGLite,DGLite,j))

            # config check
            for config in finalConf:
                print(config)

            for ip in dstAddress:
                for subnets in firewalls:
                    finder = ipaddress.ip_address(ip) in ipaddress.ip_network(subnets)
                    if finder is True:
                        device = firewalls[subnets]

            try:
                secCheck = queryYesNo('Do you want proceed on {}?: '.format(device))
                # ssh session and firewall configuration
                if secCheck is True:
                    print('Please, provide login credentials:')
                    username = input("Username: ")
                    password = getpass.getpass()
                    enable = getpass.getpass(prompt='Enable password: ')
                    hostIp = socket.gethostbyname(device)
                    print('\n' + ' CONFIGURATION IN PROGRESS '.center(50, '#') + '\n')
                    try:
                        sshSession = ConnectHandler(device_type='cisco_asa', ip=hostIp, username=username,password=password, secret=enable)
                        sshSession.send_config_set(finalConf)
                        finalConf.clear()
                        print('\n' + ' CONFIGURATION CHECK - START '.center(50, '#') + '\n')
                        print('show running-config object-group id SG-{}'.format(inventory[0]['SNOW']['change']))
                        print(sshSession.send_command('sh running-config object-group id SG-{}'.format(inventory[0]['SNOW']['change'])))
                        print('show running-config object-group id DG-{}'.format(inventory[0]['SNOW']['change']))
                        print(sshSession.send_command('sh running-config object-group id DG-{}'.format(inventory[0]['SNOW']['change'])))
                        print('show running-config access-list | i {}'.format(inventory[0]['SNOW']['change']))
                        print(sshSession.send_command('sh running-config access-list | i {}'.format(inventory[0]['SNOW']['change'])))
                        print('\n' + ' CONFIGURATION CHECK - FINISH '.center(50, '#') + '\n')

                    except Exception:
                        print('Authentication failed! Please retry')
                        sys.exit(1)
                else:
                    pass
                    print('Bye from {}!'.format(device))

            except UnboundLocalError:
                print('\n' + ' DEVICE NOT FOUND IN FIREWALL LIST '.center(50, '#') + '\n')
                sys.exit(1)

if __name__ == '__main__':
    '''Cisco ASA ACL rules configurator - Version 2.7 f180626'''
    # Catch CTRL-C
    try:
        main()
    except KeyboardInterrupt:
        print("\nCTRL-C caught, interrupting the script\n")
        sys.exit(1)

#f180513
